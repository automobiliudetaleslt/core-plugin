<?php

/**
 * @copyright C UAB NFQ Technologies
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\EventListener;

use Omni\Sylius\CorePlugin\Model\LogosAwareInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Webmozart\Assert\Assert;

/**
 * Class LogosUploadListener
 * @package \Omni\Sylius\CorePlugin\EventListener
 */
class LogosUploadListener
{
    /**
     * @var ImageUploaderInterface
     */
    private $uploader;

    /**
     * @param ImageUploaderInterface $uploader
     */
    public function __construct(ImageUploaderInterface $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @param GenericEvent $event
     */
    public function uploadLogos(GenericEvent $event): void
    {
        $subject = $event->getSubject();
        Assert::isInstanceOf($subject, LogosAwareInterface::class);

        $this->uploadSubjectLogos($subject);
    }

    /**
     * @param LogosAwareInterface $subject
     */
    private function uploadSubjectLogos(LogosAwareInterface $subject): void
    {
        $logos = $subject->getLogos();
        foreach ($logos as $logo) {
            if ($logo->hasFile()) {
                $this->uploader->upload($logo);
            }

            // Upload failed? Let's remove that image.
            if (null === $logo->getPath()) {
                $logos->removeElement($logo);
            }
        }
    }
}
