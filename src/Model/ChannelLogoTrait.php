<?php

/**
 * @copyright C UAB NFQ Technologies
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;

trait ChannelLogoTrait
{
    /**
     * @var Collection|ImageInterface[]
     */
    protected $logos;

    /**
     * ChannelLogoTrait constructor.
     */
    public function __construct()
    {
        $this->logos = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getLogos(): Collection
    {
        return $this->logos;
    }

    /**
     * {@inheritdoc}
     */
    public function getLogosByType(string $type): Collection
    {
        return $this->logos->filter(
            function (ImageInterface $logo) use ($type) {
                return $type === $logo->getType();
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function hasLogos(): bool
    {
        return !$this->logos->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasLogo(ImageInterface $logo): bool
    {
        return $this->logos->contains($logo);
    }

    /**
     * {@inheritdoc}
     */
    public function addLogo(ImageInterface $logo): void
    {
        $logo->setOwner($this);
        $this->logos->add($logo);
    }

    /**
     * {@inheritdoc}
     */
    public function removeLogo(ImageInterface $logo): void
    {
        if ($this->hasLogo($logo)) {
            $logo->setOwner(null);
            $this->logos->removeElement($logo);
        }
    }
}
