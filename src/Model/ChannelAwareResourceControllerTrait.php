<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Model;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sylius\Component\Channel\Model\ChannelsAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;

/**
 * overrides ResourceController::findOr404() method in order to take channels into account
 * for channels aware resources. Basically, it denies reaching resources in shop frontend
 * for channels that were not assigned to a channel.
 *
 * Important: should only be used in classes that extend Sylius\Bundle\ResourceBundle\Controller\ResourceController
 */
trait ChannelAwareResourceControllerTrait
{
    /**
     * @param RequestConfiguration $configuration
     *
     * @return ResourceInterface
     *
     * @throws NotFoundHttpException
     */
    protected function findOr404(RequestConfiguration $configuration): ResourceInterface
    {
        $resource = $this->singleResourceProvider->get($configuration, $this->repository);

        if (!$this->shouldResourceBeDisplayed($resource, $configuration)) {
            throw new NotFoundHttpException(sprintf('The "%s" has not been found', $this->metadata->getHumanizedName()));
        }

        return $resource;
    }

    /**
     * This method checks if resource exists at all, if it does and it is channels aware, then it
     * should contain the current channel, except if the current section is admin, because in that
     * case, it should be displayed regardless
     *
     * @param ResourceInterface $resource
     * @param RequestConfiguration $configuration
     *
     * @return bool
     */
    protected function shouldResourceBeDisplayed(ResourceInterface $resource, RequestConfiguration $configuration): bool
    {
        $currentChannel = $this->container->get('sylius.context.channel')->getChannel();
        $section = $configuration->getParameters()->get('section');
        $hasCurrentChannel = $resource instanceof ChannelsAwareInterface && $resource->hasChannel($currentChannel);

        if (null === $resource) {
            return false;
        }

        return $section == 'admin' || $hasCurrentChannel;
    }
}
