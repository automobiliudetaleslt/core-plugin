<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Form\Extension;

use Omni\Sylius\CorePlugin\Form\Type\AddressCountryCodeChoiceType;
use Sylius\Bundle\AddressingBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddressTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder->remove('countryCode');

        $builder
            ->add(
                'countryCode',
                AddressCountryCodeChoiceType::class,
                [
                    'label' => 'sylius.form.address.country',
                    'enabled' => true,
                ]
            )
            ->add(
                'houseNumber',
                TextType::class,
                [
                    'label' => 'omni_core.address.form.house_number',
                    'required' => true,
                    'constraints' => [new NotBlank(['groups' => ['omni', 'sylius']])],
                ]
            )
            ->add(
                'apartmentNumber',
                TextType::class,
                [
                    'label' => 'omni_core.address.form.apartment_number',
                    'required' => false,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType(): string
    {
        return AddressType::class;
    }
}
